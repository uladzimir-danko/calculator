package calculator;

//разбираем текст и попутно вычисляем значения функций
public class Parser {	
	private String [] expressions  = {"sqrt", "pow", "sin", "cos", "tg", "log"};
	
	public String equalsSolution(String equals)
	{
		return expressionFind(equals);
	}
	
	// вычисляем значение функции, если аргумент число
	private String expressionSolution(String expression)
	{
		int i = 0;	
		double solution = 0;
		String function, argument;
		String regex = new String("\\d+");
		function = expression.substring(0, expression.indexOf('('));
		argument = expression.substring(
				expression.indexOf('(') + 1, expression.length() - 1);
		
		while(!this.expressions[i].equals(function))
		{
			++i;
		}
		
		//если аргумент необходимо посчитать - считаем и 
		 // идем дальше искать значение функции
		if(!argument.matches(regex))
		{
			argument = expressionFind(argument);
		}
		
		switch (i)
		{
		case 0:
			solution = Math.sqrt(Double.parseDouble(argument));
			break;
		case 1:
			solution = Math.pow(Double.parseDouble(argument), 2);
			break;
		case 2:
			solution = Math.sin(Math.toRadians(Double.parseDouble(argument)));
			break;
		case 3:
			solution = Math.cos(Math.toRadians(Double.parseDouble(argument)));
			break;
		case 4:
			solution = Math.tan(Math.toRadians(Double.parseDouble(argument)));
			break;
		case 5:
			solution = Math.log(Double.parseDouble(argument));
			break;		
		}		
			
		return Double.toString(solution);
	}

	// сначала считаем значения функций
	private String expressionFind(String equation)
	{
		int indexBeginn = 0, indexEnd = 0, count = 0, capaCounter = 1;
		
		 // цикл исчет функции и передает их вместе с аргументом в expressionSolution
		 // для подсчета
		for(count = 0; count < 6; ++count)
		{
			if(equation.indexOf(expressions[count]) < 0)	{}
			else
			{
				capaCounter = 1;
				indexBeginn = equation.indexOf(expressions[count]);
				indexEnd = indexBeginn + 1;
				
				while(equation.charAt(indexEnd) != '(')
				{
					++indexEnd;
				}			
				while(capaCounter != 0)
				{
					++indexEnd;
					if(equation.charAt(indexEnd) == ')')
					{
						capaCounter--;
					}
					else
						if(equation.charAt(indexEnd) == '(')
						{
							capaCounter++;
						}				
				}
				indexEnd++;
				equation = equation.replace(equation.substring(indexBeginn, indexEnd),
						expressionSolution(equation.substring(indexBeginn, indexEnd)));
			
				--count;
			}
		}
		
		// для подсчета числового выражения воспользуемся обратной польской записью
		equation = Double.toString(PPN.eval(equation));
		return equation;
	}
}
